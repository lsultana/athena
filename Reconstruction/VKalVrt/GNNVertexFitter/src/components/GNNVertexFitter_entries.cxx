/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "GNNVertexFitter/GNNVertexFitterTool.h"
#include "src/GNNVertexFitterAlg.h"

using namespace Rec;

DECLARE_COMPONENT( GNNVertexFitterAlg )
DECLARE_COMPONENT( GNNVertexFitterTool )
